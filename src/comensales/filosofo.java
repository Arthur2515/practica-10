package comensales;

import java.util.Random;

public class filosofo implements Runnable{
    int id;
    Thread t;
    tenedor derecho; 
    tenedor izquierdo;
    int espera;
    Random r; 
    tenedor mesa[];  
    public filosofo(int id,tenedor derecho, tenedor izquierdo,tenedor mesa[]){ 
        this.id = id;
        this.derecho = derecho;
        this.izquierdo = izquierdo;
        System.out.println("El filosofo "+(id+1)+" puede usar el tenedor "+(derecho.id+1)+" y el tenedor "+ (izquierdo.id+1));
        this.mesa = mesa;
        r = new Random();
        t = new Thread(this);  
        t.start();

    }
    public void run(){
        for(int i =0;i<3;i++){ 
            synchronized(this.derecho){
                synchronized(this.izquierdo){
                    comer();
                }
            }
            pensar();
        }
        System.out.println("Filosofo "+(id+1)+" termino y no hara nada mas");
    }
    public void comer(){
        System.out.println("Filosofo "+(id+1)+ "  comera con el  tenedores "+ (this.izquierdo.id+1)+" y "+(this.derecho.id+1)); 
        derecho.tomar(id); 
        izquierdo.tomar(id);
        espera = r.nextInt(5000); 
        System.out.println(" ------------- Filosofo "+(id+1)+ " comiendo --------------");
        try{
            Thread.sleep(espera);
        }catch(InterruptedException e){
        } 
        derecho.soltar();
        izquierdo.soltar();
        System.out.println("Filosofo "+(id+1)+ " termino de comer y libero tenedores "+ (this.izquierdo.id+1)+" y "+(this.derecho.id+1));
    }
    public void pensar(){
        espera = (r.nextInt(5000));
         try{
            System.out.println(" El Filosofo " + (id+1)+ " comiensa a pensar");
            Thread.sleep(espera);
         }catch(InterruptedException e){
         } 
    }
    public String checar_mesa(){  
        String a="";
        for(int i =0;i<mesa.length;i++){
            a = a+("["+mesa[i].ban+"]");
        }
        return a;
    }
}